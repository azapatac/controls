﻿using Controls.Views;
using Xamarin.Forms;

namespace Controls
{
    public partial class App : Application
    {
        public App()
        {
            Device.SetFlags(new string[]
            {
                "CarouselView_Experimental",
                "Expander_Experimental",
                "MediaElement_Experimental",
                "RadioButton_Experimental",
                "Shapes_Experimental",
                "SwipeView_Experimental"
            });

            InitializeComponent();
            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}