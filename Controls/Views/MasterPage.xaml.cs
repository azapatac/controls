﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Controls.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage : ContentPage
    {
        public ListView ListView;

        public MasterPage()
        {
            InitializeComponent();

            BindingContext = new MyMasterDetailPageMasterViewModel();
            ListView = MenuItemsListView;
        }

        class MyMasterDetailPageMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<MenuItemPage> MenuItems { get; set; }

            public MyMasterDetailPageMasterViewModel()
            {
                MenuItems = new ObservableCollection<MenuItemPage>(new[]
                {
                    new MenuItemPage { Id = 0, Title = "Present data", TargetType = typeof(PresentDataPage)},
                    new MenuItemPage { Id = 1, Title = "Initiate command", TargetType = typeof(InitiateCommandPage) },
                    new MenuItemPage { Id = 2, Title = "Set values", TargetType = typeof(SetValuesPage) },
                    new MenuItemPage { Id = 3, Title = "Edit text", TargetType = typeof(EditTextPage) },
                    new MenuItemPage { Id = 5, Title = "Indicate activity", TargetType = typeof(IndicateActivityPage) },
                    new MenuItemPage { Id = 6, Title = "Display collections", TargetType = typeof(DisplayCollectionsPage) },
                    new MenuItemPage { Id = 7, Title = "Additionals controls", TargetType = typeof(AdditionalsControlsPage) },
                });
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }
    }
}
