﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Controls.Views
{
    public partial class SetValuesPage : ContentPage
    {
        public SetValuesPage()
        {
            InitializeComponent();
        }

        void Slider_ValueChanged(System.Object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            myLabel.Text = ((Slider)sender).Value.ToString();
        }

        void Stepper_ValueChanged(System.Object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            myLabel1.Text = ((Stepper)sender).Value.ToString();
        }
    }
}
