﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Controls.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
        }

        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MenuItemPage;
            if (item == null)
                return;

            var page = (Page)Activator.CreateInstance(item.TargetType);
            page.Title = item.Title;

            Detail = new NavigationPage(page) { BackgroundColor = Color.WhiteSmoke };
           
            IsPresented = false;

            MasterPage.ListView.SelectedItem = null;
        }
    }
}