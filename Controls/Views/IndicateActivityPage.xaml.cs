﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Controls.Views
{
    public partial class IndicateActivityPage : ContentPage
    {
        public IndicateActivityPage()
        {
            InitializeComponent();
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            _ = Start();
        }

        private async Task Start()
        {
            myProgressBar.Progress = 0;
            await myProgressBar.ProgressTo(1, 2000, Easing.Linear);
        }
    }
}
