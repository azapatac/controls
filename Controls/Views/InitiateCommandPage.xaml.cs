﻿
using Xamarin.Forms;

namespace Controls.Views
{
    public partial class InitiateCommandPage : ContentPage
    {
        public InitiateCommandPage()
        {
            InitializeComponent();
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            App.Current.MainPage.DisplayAlert(".Net University", "Button", "Ok");
        }

        void ImageButton_Clicked(System.Object sender, System.EventArgs e)
        {
            App.Current.MainPage.DisplayAlert(".Net University", "ImageButton", "Ok");
        }

        void RefreshView_Refreshing(System.Object sender, System.EventArgs e)
        {
            refreshView.IsRefreshing = true;
            App.Current.MainPage.DisplayAlert(".Net University", "RefreshView", "Ok");
            refreshView.IsRefreshing = false;
        }

        void SearchBar_SearchButtonPressed(System.Object sender, System.EventArgs e)
        {
            App.Current.MainPage.DisplayAlert(".Net University", "SearchBar", "Ok");
        }        
    }
}
