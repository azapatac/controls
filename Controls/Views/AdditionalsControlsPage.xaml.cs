﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Controls.Views
{
    public partial class AdditionalsControlsPage : ContentPage
    {
        public AdditionalsControlsPage()
        {
            InitializeComponent();
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            _ = DisplayPrompt();
        }

        void Button_Clicked_1(System.Object sender, System.EventArgs e)
        {
            _ = DisplayActionSheet();
        }

        private async Task DisplayActionSheet()
        {
            myEntry.Text = await App.Current.MainPage.DisplayActionSheet("DisplayActionSheet", "Cancel", "Delete", "Option 1", "Option 2", "Option 3", "Option 4");
        }

        private async Task DisplayPrompt()
        {
            myEntry.Text = await App.Current.MainPage.DisplayPromptAsync("DisplayPromptAsync", "Enter email", "Ok", "Cancel", "Placeholder", 20, Keyboard.Email);
        }
        
    }
}
