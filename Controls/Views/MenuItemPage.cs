﻿using System;

namespace Controls.Views
{
    public class MenuItemPage
    {        
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}